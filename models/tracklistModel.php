<?php
/** Tracklist class. Each object is a single tracklist that controls all tracks it 
* contains
* @var 	$tlid int from db.tracklist.tlid
* @var	$name string from db.tracklist.name
* @var	$owner string from db.tracklist.owner
* @var	$priv int from db.tracklist.private
* @var	$tracks array of Tracks/Video objects
* 
*/

class Tracklist {
	protected $tlid;
	protected $name;
	protected $description;
	protected $owner;
	protected $priv;
	protected $tracks;

/** 
 * @param $listData Tracklist data as row from db
 * @param $videos array of Video objects
 */
	
	function __construct ($listData, $videos) {
		$this->tlid = $listData['tlid'];
		$this->name = $listData['name'];
		if (isset($listData['description'])) {
			$this->description = $listData['description'];
		} else {
			$this->description = NULL;
		}
		$this->owner =$listData['owner'];
		$this->priv = $listData['private'];
		
		$this->tracks = $videos;		
	}

/** @return integer that corresponds with unique tracklist.tlid in db
*/
	public function getTlId() {
		return $this->tlid;
	}

/** @return string that corresponds with tracklist.name in db
*/
	public function getName() {
		return $this->name;
	}

/** @return string that corresponds with tracklist.description in db
*/
	public function getDescription() {
		return $this->description;
	}

/** @return string that corresponds with tracklist.owner in db, unique user id
*/
	public function getOwner() {
		return $this->owner;
	}

/** @return string that corresponds with tracklist.private in db
*/
	public function getPriv() {
		return $this->priv;
	}
	
/** @return array of Video objects connected with tracklist
*/
	public function getTracks() {
		return $this->tracks;
	}

/** This function is used to echo HTML in columns 1 and 2 in homeview for registered users
* for optimalization, could be replaced with $this->prepareTracklistInfo() and new View class method
*/
	function displayTracklist() {
		echo '<div class="row">
			<div class="col-sm-4 col-md-4">
				<p>Name: '.$this->name
				.'<br>Description: '.$this->description
				.'</p>
				<form class="form-horizontal" method="post" action="'.$_SERVER['PHP_SELF'].'">
				  <input type="hidden" name="tlid" value="'.$this->tlid.'"/>
				  <div class="form-group">
					<div class="col-md-4 col-sm-4">						
						<button name="editTL" type="submit" class="btn btn-primary">Edit</button>						
					</div>
					<div class="col-md-4 col-sm-4">						
						<button name="removeTL" type="submit" class="btn btn-danger">Remove</button>						
					</div>
				  </div>
				</form>
			</div>
			<div class="col-sm-8">
			<p>Number of tracks: '.count($this->tracks);
			foreach ($this->tracks as $index=>$videoObject) {
				echo '<div class="row">';
				echo '<div class="col-sm-4 col-md-4">Name: '.$videoObject->retName();
				echo '</div><div class="col-sm-4 col-md-4">Path: '.$videoObject->retPath();
				echo '</div><div class="col-sm-8 col-md-8">Desc: '.$videoObject->retDesc();
				echo '</div></div>';
			}
		echo '</div>			
		</div><div class="row"><div class="col-sm-12 col-md-12"><hr></div></div>';
	}

/** Function is used to prepare basic info about a tracklist and its tracks
 * Returned values used mostly to to be sent forward to View
 *
 * $res['0'] tracklist id
 * $res['1'] tracklist name
 * $res['2'] tracklist description
 * $res['3'] array [$trackId] => $trackName
 *  
 */ 	
	public function prepareTracklistInfo() {
		$res = array();
		array_push ($res, $this->tlid);
		array_push ($res, $this->name);
		array_push ($res, $this->description);
		
		$trackNames = array();
		foreach ($this->tracks as $index=>$videoObject) {
			$name = $videoObject->retName();
			$id = $videoObject->retId();
			$trackNames[$id]=$name;
		}
		array_push ($res, $trackNames);
		return $res;
	}
	
}