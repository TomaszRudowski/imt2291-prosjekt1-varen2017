<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
  class Video {
      private $id;
      private $path;
      private $name;
      private $desc;
	  private $addTime;
      private $subs;


      public function __construct($i, $p, $n, $d, $t, $subs) {
          $this->path = $p;        // Video path
          $this->name = $n;        // Video name
          $this->desc = $d;        // Video description
          $this->id = $i;          // Video id
          $this->addTime = $t;     // timestamp from database
          $this->subs = $subs;     // Array of subtitles

    }

    public function boxView() {

      $url = 'http://'.$_SERVER['HTTP_HOST'];
      $html = "<div class='col-md-4 col-xs-6'><a href='".$url."/video/".$this->id."'><h3>".$this->name."</h3><p>".$this->desc."</p></a></div>";
      return $html;
    }

    public function addTrackView() {
      echo "<h3>{$this->name}</h3>";
      echo "<p>{$this->desc}</p>";
      echo "<form method='post' >";
      echo "<button  name='deleteTrack' value='{$this->id}' type='submit' class='btn btn-default btn-lg'>
          <span class='glyphicon glyphicon-trash' aria-hidden='true'></span> Slett
            </button>";
      echo "<button name='editTrack' value='{$this->id}' type='submit' class='btn btn-default btn-lg'>
          <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> Endre
            </button>";
      echo "</form>";
    }

    public function retName() {
      return $this->name;
    }

    public function retPath() {
      return $this->path;
    }

    public function retDesc() {
      return $this->desc;
    }

    public function retId() {
      return $this->id;
    }

      public function retSubs() {
          return $this->subs;
      }

	public function retTime () {
	  return $this->addTime;
	}
/** returns an array() { ["id"]=> $id ["video"]=> $path ["description"]=> $desc
 * ["name"]=> $name ["addTime"]=> $addTime }
 *
 */
	public function retAsRow () {
		$res = array();
		$res['id'] = $this->id;
		$res['video'] = $this->path;
		$res['description'] = $this->desc;
		$res['name'] = $this->name;
		$res['addTime'] = $this->addTime;
		return $res;
	}
  }

