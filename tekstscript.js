// https://msdn.microsoft.com/en-us/library/bg123962(v=vs.85).aspx
// http://stackoverflow.com/questions/10328210/javascriptdiv-appendchild

var video = document.getElementById("vidspiller");
var textTracks = video.textTracks;

// Uferdig funksjon som skal synce rødfarge på trykkbare cues med activeCues
/*function activeCueChange() {
  for (var i = 0; i < textTracks.length; i++) { // For hver track
    var textTrack = textTracks[i];
    texTrack.addEventListener("cuechange"), function () { // legg til listener
      var aCues = texTrack.activeCues; // Finn active cues
      console.log(aCues);
      var div = document.getElementById("loadtekstdiv"); // finn riktig div
      var lister = div.getElementByClassName("ul"); // Alle textTrack-listene
      for (var i = 0; i < lister.length; i++) { // For hver liste
        var listitems =  lister[i].getElementsbyClassName(li); // Finn items
        for (var j = 0; j < listitems.length; j++) { // For hvert item
          var anchors = listitems[j].getElementsbyClassName(a); // Finn anchors
          if (anchors[j].innerHTML == aCues[0].text) { // Hvis teksten samsvarer
            anchors[j].style.color = "red";            // med activeCue
          }                                            // blir fargen blå
          else {                              // for alle andre anchors
            anchors[j].style.color = "blue";  // blir fargen rød
          }
        }
      }
    }
  }
}*/

// funksjon som automatisk laster inn alle textTracks tilhørende videoen
// og gjør cues tilhørende den første textTracken synlig
window.onload = function loadtekst() {
  var loadtekstdiv = document.getElementById("loadtekstdiv");
  for (var i = 0; i < textTracks.length; i++) { //for hver track

      var list = document.createElement("ul");
      list.label = "liste" + (i+1);
      if (i != 0) { // Kun første element synlig
        list.style.display = "none";
      }

      var textTrack = textTracks[i];
      textTrack.mode = "showing";   // Måtte skifte til showing
      var cueList = textTrack.cues; // får at cueList ikke skulle bli null
      textTrack.mode = "hidden";
      cueList.length = textTrack.cues.Length;
      for (var j = 0; j < cueList.length; j++) {
        cue = cueList[j];
        cueText = cue.text;
        cueStart = cue.startTime;

        var listItem = document.createElement("li");
        var anchorItem = document.createElement("a");

        anchorItem.innerHTML = cueText;
        anchorItem.href = "#";
        anchorItem.setAttribute("onclick", "setTime(" + cueStart + "); return false;");

        listItem.appendChild(anchorItem);
        list.appendChild(listItem);
      }
      loadtekstdiv.appendChild(list);
  }
  trackVelger();
//  activeCueChange();
}

// Lager knapper for valg av textTrack
function trackVelger() {
   var loadtekstdiv = document.getElementById("loadtekstdiv");
   if (textTracks.length > 1) {
     for (var i = 0; i < textTracks.length; i++) {
       textTrack = textTracks[i];
       var knapp = document.createElement("button");
       knapp.innerHTML = textTrack.label; //Erstatt med tracklabel
       console.log(textTrack.label);
       knapp.id = i;
       knapp.setAttribute("onclick", "buttonFiller(" + knapp.id + "); return false;");
       loadtekstdiv.appendChild(knapp);
     }
   }
}
// Endrer tidspunkt i videoen til starten av cue
function setTime(newTime) {
  video.currentTime = newTime;
}
// Funksjon som viser listen som ligger [index] i rekkefølgen med lister
// og gjemmer de andre texttrack-listene
function buttonFiller(index) {
  var tekstdiv = document.getElementById("tekstdiv"); // Finner område
  var lister = tekstdiv.getElementsByTagName("ul");   // Til å lete etter lister
  for (var i = 0; i < lister.length; i++) {
    if (i == index) {
      lister[i].style.display= "block";
    }
    else {
      lister[i].style.display= "none";
    }
  }
}
