<?php

function noLoginButtons() {
  return '<li><a href="#" data-toggle="modal" data-target="#myModal2">Logg inn</a></li>
          <li><a href="#" data-toggle="modal" data-target="#myModal">Register</a></li>';
}

function loggedInButtons() {
  return '<li><a href="?logout=1">Logg out</a></li>';
}

function generateLogin($loginStatus) {
  if (!$loginStatus) {
    $a = noLoginButtons();
  }
  else  {
    $a = loggedInButtons();
  }
  $html = <<<EOT
  <nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ForeNett</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
          <li><a href="#">Link</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">


      <form class="navbar-form navbar-left">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
      </form>
        $a
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Sign In</h4>
		      </div>
		      <div class="modal-body">
					<form class="form-register" method="post">
					<label for="inputEmail" class="sr-only">Email address</label>
					<input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required autofocus/>
					<label for="inputPassword" class="sr-only">Password</label>
					<input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required/>
					<div class="checkbox">
						<label>
							<input name="remember" type="checkbox" value="remember"> Remember me
						</label>
					</div>
					<button name="lbtn" value="1" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
					</form>
							<button type="button" class="btn btn-default btn-block btn-lg" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Sign up</h4>
		      </div>
		      <div class="modal-body">
						<form class="form-register" method="post">
								<h2 class="form-register-heading">Sign up</h2>
								<input type="email" id="inputEmail2" class="form-control" name="email" placeholder="Email address" required autofocus/>
								<input type="password" name="password" placeholder="Password" class="form-control" required>
								<input type="text" name="givenname" placeholder="Name" class="form-control" required>
								<input type="text" name="surname" placeholder="Surname" class="form-control" required>
								<button name="rbtn" type="submit"  class="btn btn-lg btn-success btn-block">Sign up</button>
								<button type="button" class="btn btn-default btn-block btn-lg" data-dismiss="modal">Close</button>
						</form>
		      </div>
		    </div>
		  </div>
		</div>
EOT;
return $html;
}
?>
