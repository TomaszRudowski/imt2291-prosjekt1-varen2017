<?php
/** View class inholds only methods, that help to format HTML output
* look at ../partial for HTML-components
* use file_get_contents('../partial/header.php') ??
*/

error_reporting(-1);
ini_set('display_errors', 'On');
class View {

  public function __construct()
    {

}
    /**
       Generates a navigation bar on top of page
     */
    public function generateNav() { 	// OK
        $url ="http://". $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
            if(isset($_SESSION['uid'])){		// Det er satt en bruker ID
            if ($_SESSION['uid'] != 'guest') {		// Om ikke gjest, så er det en innlogget bruker
                $a = '<li><a href="?logout=1">Logg out</a></li>';	// Viser loggutknapp
                $linker = array('Home' => "$url", 'Legg Til Video' => "$url?page=addtrack");
            } else {	// Ellers er man gjest. Da blir man vist registrering og innlogging
                $linker = array('Home' => "$url");
                $a = '<li><a href="#" data-toggle="modal" data-target="#myModal2">Logg inn</a></li><li><a href="#" data-toggle="modal" data-target="#myModal">Register</a></li>';
                }
            }
            else {
                $a = '<li><a href="#" data-toggle="modal" data-target="#myModal2">Logg inn</a></li><li><a href="#" data-toggle="modal" data-target="#myModal">Register</a></li>';
            }
            // Følgende kode lager Navigasjon
            $html = '<nav class="navbar navbar-inverse navbar-static-top navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ForeNett</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">';
                  foreach($linker as $navn => $link){
                      $html = $html."<li><a href='$link'>$navn</a></li>";
                    }
            $html = $html.'</ul>
                <ul class="nav navbar-nav navbar-right">
                <form class="navbar-form navbar-left" method="get" >
                    <div class="form-group">
                    <input name="searchWord" type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default" value="search">Submit</button>
                </form>'.
                $a.'
                </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
            </nav>
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Sign In</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-register" method="post">
                            <label for="inputEmail" class="sr-only">Email address</label>
                            <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required autofocus/>
                            <label for="inputPassword" class="sr-only">Password</label>
                            <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required/>
                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="remember"> Remember me
                                </label>
                            </div>
                            <button name="lbtn" value="1" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                            </form>
                                    <button type="button" class="btn btn-default btn-block btn-lg" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>
                </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Sign up</h4>
                        </div>
                        <div class="modal-body">
                                <form class="form-register" method="post">
                                        <h2 class="form-register-heading">Sign up</h2>
                                        <input type="email" id="inputEmail2" class="form-control" name="email" placeholder="Email address" required autofocus/>
                                        <input type="password" name="password" placeholder="Password" class="form-control" required>
                                        <input type="text" name="givenname" placeholder="Name" class="form-control" required>
                                        <input type="text" name="surname" placeholder="Surname" class="form-control" required>
                                        <button name="rbtn" type="submit"  class="btn btn-lg btn-success btn-block">Sign up</button>
                                        <button type="button" class="btn btn-default btn-block btn-lg" data-dismiss="modal">Close</button>
                                </form>
                        </div>
                    </div>
                    </div>
                </div><div class="container" id="cont">';
        return $html;	// Returnerer HTML
    }



public function header() {	 // OK
    $html = '<head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta name="theme-color" content="#333">
                    <title>WWW-Prosjekt</title>
                    <meta name="author" content="Martin Klingenberg, Tomasz Rudowski, Martin Trehjøringen">
                    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="/CSS/main.css">
    </head><body>';
    return $html;
}
    /**Makes a footer to the webpage
     */
public function bottomLine() {	 // OK  // </div> close container
    $html =' 
                </div></div><footer><div class="container"><div class="col-md-4">
    <h3>Laget av</h3><p>Martin Klingenberg</p><p>Martin Trehjøringen</p><p>Tomasz Rudowski</p></div>
<div class="col-md-4"><h3>Laget med</h3><p>PHP</p><p>Javascripy</p><p>Sinne</p><p>Mangel på livslyst, og en liten klype dårlig humor</p></div><div class="col-md-4">
<h3><a href = "https://bitbucket.org/TomaszRudowski/imt2291-prosjekt1-varen2017/wiki/Home" >Sjekk ut wikien</a></h3><p>Av opplagte årsaker</p></div>
    </footer></div>
                </body>
                </html>';
    return $html;
}


/** Template for a page. Not used currently.
*/
	public function homePage () {
		echo $this->header();
		echo $this->generateNav();
		// here comes page body
		echo $this->bottomLine();
	}

/**
 * $ownedTracks  array( array([name]=>'vid1', [id]=>xxx),array([name]=>'vid2', [id]=>xxx ),...
 */
	public function generateNewTracklistForm($ownedTracks) {
		$html = '<form class="form-horizontal" method="post" action="'.$_SERVER['PHP_SELF'].'">
				<h3>Create new tracklist</h3>
				<div class="form-group">
					<label class="control-label col-sm-4" for="name">List name:</label>
					<div class="col-sm-8"><input type="text" class="form-control" name="name"></div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="description">Description:</label>
					<div class="col-sm-8"><input type="text" class="form-control" name="description"></div>
				</div>
				<h4 class="col-sm-offset-2">Videos currently available to add</h4>
				<p class="col-sm-offset-2">(Check to add)</p>
				<div class="form-group">';
		foreach ($ownedTracks as $myTrack) {
			$html .= '<div class="checkbox"> <label class="col-sm-offset-2">
				<input type="checkbox" name="tracksToAdd[]" value="'.$myTrack['id'].'">'.$myTrack['name'].'
			</label> </div>';
		}
		$html .= '</div>
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-8">
						<input name="addTL" type="submit" class="btn btn-success" value="Create tracklist"/>
					</div>
				</div>
				</form>';
	return $html;
	}

/**
 * $tlid['0'] tracklist id
 * $tlid['1'] tracklist name
 * $tlid['2'] tracklist description
 * $tlid['3'] array [$trackId] => $trackName
 * $ownedTracks  array( array([name]=>'vid1', [id]=>xxx),array([name]=>'vid2', [id]=>xxx ),...
*/
	public function generateEditTracklistForm($tlid, $ownedTracks) {
		//var_dump($ownedTracks);	// nyttig for testing :)
		$tracks = $tlid[3];
		$html = '<form class="form-horizontal" method="post" action="'.$_SERVER['PHP_SELF'].'">
				<h2 class="col-sm-offset-2">'.$tlid['1'].'</h2>
				<div class="form-group">
					<label class="control-label col-sm-4" for="name">List name:</label>
					<div class="col-sm-8"><input type="text" class="form-control" name="name"
					value="'.$tlid[1].'"></div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="description">Description:</label>
					<div class="col-sm-8"><input type="text" class="form-control" name="description"
					value="'.$tlid[2].'"></div>
				</div>
				<h4 class="col-sm-offset-2">Videos currently in "'.$tlid['1'].'"</h4>
				<p class="col-sm-offset-2">(Uncheck to remove)</p>
				<div class="form-group">';
		foreach ($tracks as $trackId => $trackName) {
			$html .= '<div class="checkbox"> <label class="col-sm-offset-2">
				<input type="checkbox" name="tracksToAdd[]" value="'.$trackId.'" checked>'.$trackName.'
				</label> </div>';
		}
		$html .= '</div>
				<h4 class="col-sm-offset-2">Videos currently available to add</h4>
				<p class="col-sm-offset-2">(Check to add)</p>
				<div class="form-group">';
		foreach ($ownedTracks as $myTrack) {
			$html .= '<div class="checkbox"> <label class="col-sm-offset-2">
				<input type="checkbox" name="tracksToAdd[]" value="'.$myTrack['id'].'">'.$myTrack['name'].'
			</label> </div>';
		}
		$html .= '</div>

				<div class="form-group">
					<div class="col-sm-offset-0 col-sm-4 col-md-4">
						<button name="editTL" type="submit" class="btn btn-danger" >Reverse changes</button>
						<input type="hidden" name="tlid" value="'.$tlid['0'].'"/>
					</div>
					<div class="col-sm-offset-4 col-md-offset-2 col-sm-4 col-md-4">
						<button name="changeTL" type="submit" class="btn btn-success" >Submit changes</button>
					</div>
				</div>
				</form>';
		return $html;
	}

/** Create <p> with info about a single video
*
* @param $track is an array() { ["id"]=> ... ["video"]=> ... ["description"]=> ...
* ["name"]=> ... ["addTime"]=> ... }
*
*/
	public function generateTrackInfoView ($track) {
		$html = '<p><b><a href="index.php?video='.$track['id'].'">'.$track['name'].'</a></b>
		  <br><i>(Video added :'.$track['addTime'].')</i>';
		if (isset($track['description'])) {
			$html .= '<br>Description: '.$track['description'];
		}
		$html .= '</p>';
		return $html;
	}

/** Create <p> with info about a single tracklist
*
* @param  $tracklist is an array() { ["tlid"]=> ... ["name"]=> ... ["description"]=> ...
 * ["addTime"]=> ... }
 *
 */
	public function generateTracklistInfoView ($tracklist, $searchWord, $noOfTracks=NULL) {
		$html = '<p><b><a href="index.php?watchList='.$tracklist['tlid'].'&searchWord='.$searchWord.'">'.$tracklist['name'].'</a></b>
			<br><i>(List updated :'.$tracklist['addTime'].')</i>';
		if (isset($tracklist['description'])) {
			$html .= '<br>Description: '.$tracklist['description'];
		}
		if (isset($noOfTracks)) {
			$html .= '<br>Playlist inholds ';
			switch ($noOfTracks) {
				case '0' : $html .= 'no videos'; break;
				case '1' : $html .= 'one video'; break;
				default :  $html .= $noOfTracks.' videos'; break;
			}
		}
		$html .= '</p>';
		return $html;
	}

  public function trackView($video) {
    echo $this->header();
    echo $this->generateNav();
    $html = '
    <div class="container" id="vidmedtekst"> <!-- Herunder kommer videospilleren-->
      <div class="row">
        <div class="col-md-6">
          <h1>'.$video->retName().'</h1>
        </div>
        <div class="col-md-6">
          <h1>Her kommer subtitle-valg</h1>
        </div>
      </div>
        <div class="row">
          <div id="videodiv" class="col-md-6">
            <video id="vidspiller">
              <script>
                // Grab a handle to the video
                var video = document.getElementById("vidspiller");
                // Turn off the default controls
                video.controls = false;
              </script>
              <source src="'.$video->retPath().'" type="video/mp4">';
              foreach($video->retSubs() as $sub => $key) {
                $html = $html . "<track Label='$sub' src='$key' kind='subtitles' scrlang='en'>";
              }
            $html = $html.'</video>
            <div id="vidcontrols">
              <!--progress-bar-->
              <div id="hp_slide">
                <div id="hp_range"></div>
              </div>
              <!-- Play/pause-knapp-->
              <button id="playpause" title="play" onclick="togglePlayPause()">&#9658;</button>
              <!-- Hastiget-selector-->
              <select id="hastighet">
                <option value="slow">0.5x</option>
                <option value="normal" selected="selected">1.0x</option>
                <option value="fast">1.5x</option>
              </select>
              <!--mute-knapp-->
              <button id="mute" onclick="toggleMute()">&#128263</button>
              <!--volum-slider-->
              <input id="volume" min="0" max="1" step="0.1" type="range" onchange="setVolume()"/>
              <p>'.$video->retDesc().'</p>
            </div>
          </div>
          <div id="tekstdiv" class="col-md-6">
            <div id="trackvelgerdiv"></div>
            <div id="loadtekstdiv"></div>
          </div>
        </div>
      </div>
    <script type="text/javascript" src="videoscript.js"></script>
    <script type="text/javascript" src="tekstscript.js"></script>';
    echo $html;
    echo $this->bottomLine();
  }



    public function addTrack($videoList, $vidID=null) {
      echo "<div class='row'>";
      echo "<div class='col-md-6'>"; // track list column

      foreach($videoList as $video => $key) {
          echo "<div class='video'>";
          $key->addTrackView();
          echo "</div>";
      }
      echo "</div>";                // End track list column

      echo "<div class='col-md-6'>"; // add track form
      echo "<form method='post' enctype='multipart/form-data'>";
      if($vidID) {  // A file is going to be edited
        $selVideo;
        foreach($videoList as $vid => $key) {
          if($vidID == $key->retId()) $selVideo = $key;
        }
        echo "<h3>Endre Track</h3><label>Navn</label><input type='text' class='form-control' name='vidname' value='{$selVideo->retName()}'>
       <label>Beskrivelse</label><textarea name='viddesc' class='form-control'>{$selVideo->retDesc()}</textarea>
<button type='submit' name='updateTrack' class='btn btn-default btn-lg' value='$vidID'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span>  Oppdater</button></form>";
            echo "<h3>Legg til undertekst</h3>";
            echo "<form enctype='multipart/form-data' method='post'><div class='form-group'>";
            echo "<label>Språk</label><input type='text'  class='form-control' placeholder='eks: Norsk' name='vttname'>";
            echo "<label>VTT fil</label><input value='Upload' name='vttfil' type='file'/><button name='vttfil'  class='btn btn-default btn-lg'type='submit'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span>  Submit</button></div><input type='hidden' name='vidid' value='{$vidID}'/></form>";
            echo "<div class='tekster'>";
            echo "<h3>Undertekster</h3>";
            foreach($selVideo->retSubs() as $sub => $key) {
                echo "<form method='post'>";
                echo "<label><h4>$sub</h4></label>";
                echo "<input type='hidden' name='vidid' value='{$vidID}'/>";
                echo "<button name='deleteSub' type='submit' value='$key'> <span class='glyphicon glyphicon-trash' aria-hidden='true'></span> Remove </button>";
                echo "</form>";
            }
            echo "</div></div>";

      } else {
          echo "<h3>Legg til Video</h3><label>Navn</label><input type='text' class='form-control' name='vidname' placeholder='Video name'>
       <label>Beskrivelse</label><textarea name='viddesc' placeholder='Video description' class='form-control'></textarea>
       <label>Video File</label><input value='Upload' name='uploaded' type='file'/><button  class='btn btn-default btn-lg'type='submit'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span>  Submit</button></div></form>";
      }
      echo "</div>";                // end track form
   }

/** Generate student homePage
*
*/
   public function generateGuestHomePage($newestTracks) {
	 $html = $this->header();
	 $html .= $this->generateNav();
	 $html .= '<div><h4>This is student homepage</h4>
		<p> You are unfortunatelly not allowed to upload videos	but You can search our
		database to find those Your teacher shared.</p>
		</div>
		<div>
		<h4>Here are newest videofiles that were uploaded to database</h4>';
	 foreach ($newestTracks as $key=>$track) {
		$html .= $this->generateTrackInfoView($track);
	 }
	 $html .= '</div>';
	 return $html;
   }


}
