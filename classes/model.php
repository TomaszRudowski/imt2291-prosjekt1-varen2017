<?php
/* Model class to handle database operations
* Uses 'admin/settings.php' to determine admin user and password to operate on db
* @var $dbPDO PDO object to current database
* @var $admin array('name'=>'...' ,'pwd'=>'...'), set by 'admin/settings.php' 
*
* @param $dbName string send forward by Controller on start
*/
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once 'models/videoModel.php';
require_once 'models/tracklistModel.php';

class Model {
    protected $dbPDO;
  protected $admin;

  public function __construct($dbName)
    {
    require_once('admin/settings.php');
    $this->openConnection($dbName);
    $this->dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

/** Creates PDO object based on info from __construct
*/
  function openConnection($dbName) {
    try {

		$this->dbPDO = new PDO ("mysql:host=localhost;dbname=$dbName",$this->admin['name'], $this->admin['pwd']);
		} catch (PDOException $e) {
			echo $e;
			die ('Unable to connect to database, please try again later');
		}
	}


  /** Function returns admin data (used to setup db in createEnvironment.php)
*
* @return array() returns admindata as an assosiative array
*/
  function getAdminData () {
    return $this->admin;
  }


/** Function finds user data in db based on given $email
*
* @param string $email  user email
*
* @return print_r($key);array() returns userdata as an assosiative array
*/
  function getUserDataByEmail ($email) {
    $sql = "SELECT id, email, givenname, surename, password FROM user WHERE email=?";
    try {
        $stm = $this->dbPDO->prepare($sql);
        $stm->execute(array($email));
        return $stm->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {}

  }

/** Function finds user data in db based on given $uid
*
* @param integer $uid user Id in db
*
* @return array() returns userdata as an assosiative array
*/
  function getUserDataById ($uid) {
    $sql = "SELECT id, email, givenname, surename, password FROM user WHERE id=?";
    try {$stm = $this->dbPDO->prepare($sql);
    $stm->execute(array($uid));
    return $stm->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {}
  }

/** Function inserts login data in database. It should be called only as a result of
* submiting login form with 'remember me' checkbox on.
*
* @param integer $uid user id
* @param string $identifier md5 encoded user name and time
* @param string $token md5 encoded logintime
*/
  function setPersistedLogin($uid,$identifier, $token) {
    /* to consider: if exist allready, it means
    * 1. expired - (replace: remove and continue?) should we have auto cleaning in db?
    * 2. user log in from different device -
    *   2.1 expired - (replace: remove and continue?)
    * 2.2 not expired - (ask user if replace / replace without asking)
    */
    $sql = "INSERT INTO persistedlogin (uid, identifier, token) VALUES (?, ?, ?)";
    try {$sth1 = $this->dbPDO->prepare($sql);
        $sth1->execute (array ($uid, $identifier, $token));
    } catch (Exception $e) {}
  }

/** Function finds login data in database and return 'token' to compare
* @param integer $uid user id
* @param string $identifier md5 encoded user name and time
*
* @return assosiative array('token'=> )
*/
  function findPersistedLogin($uid, $identifier) {
    $sql = "SELECT token FROM persistedlogin WHERE uid=? AND identifier=?";
    try {$stm = $this->dbPDO->prepare ($sql);
        $stm->execute (array ($uid, $identifier));
        return $stm->fetch(PDO::FETCH_ASSOC);
    }catch (Exception $e) {}
  }

/** Function updates (with new expiretime) login data in database after succesful use
* of cookie to login
*
* @param integer $uid user id
* @param string $identifier md5 encoded user name and time
* @param string $token md5 encoded logintime
*/
  function updatePersistedLogin($uid,$identifier, $token) {
    $sql = "UPDATE persistedlogin set token=? where uid=? and identifier=?";
    try {$stm = $this->dbPDO->prepare($sql);
        $stm->execute (array ($token, $uid, $identifier));
    } catch (Exception $e) {}
  }

/** Function removes login data from database
*
* @param integer $uid user id
*/
  function removePersistedLogin($uid) {
    $sql = "DELETE FROM persistedlogin WHERE uid='?'";
    try {
        $stm = $this->dbPDO->prepare($sql);
    $stm->execute (array ($uid));
    } catch (Exception $e) {}
  }

/** Function adds userdata to database if email is unique,
*
* @param string $email plain text new unique user email
* @param string $pwdHash hashed by Controller user password
* @param string $givenname plain text new user givenname
* @param string $surename plain text new user surename
*
* @return associative array that contains only ('uid'=>..) or error message ('error'=>'description')
*/
	function addUser ($email, $pwdHash, $givenname, $surename) {		if ($this->getUserDataByEmail($email)) {
			return (array ('error'=>'email address already registered'));
		} else {
			$sql = 'INSERT INTO user (id, email, password, surename, givenname) VALUES (UUID(), ?, ?, ?, ?)';
            try {
                $stm = $this->dbPDO->prepare ($sql);
                $stm->execute (array ($email, $pwdHash, $surename, $givenname));
                if ($stm->rowCount()==0) {
                    return (array ('error'=>'email unique but not added of other reason'));
                } else {
                    $res = $this->getUserDataByEmail($email);  //need to get UUID() value maybe new function?
                    return array('uid'=>$res['id']);
                }
            }catch (Exception $e) {}
      } 

	}

/** Function inserts added video file info into db
* @param string $trackpath url to video file
* @param string $description
* @param string $name
*/
	public function addTrack ($trackpath, $description, $name) {
		var_dump( $trackpath);
		$sql = 'INSERT INTO track (id, video, description, name, owner) VALUES (UUID(), ?, ?, ?, ?)';
		$stm = $this->dbPDO->prepare ($sql);
		$stm->execute ( array($trackpath, $description, $name, $_SESSION['uid']));
		if ($stm->rowCount()==0) {
			echo "kunne ikke legge til linjer i databasen";
		}
	}

    /**
       Gets a set number of recent tracks
       @param integer $number - 
     */
  public function getTracksRecent($number) {
      $sql = 'SELECT id FROM track LIMIT ?';
      try {
          $stm = $this->dbPDO->prepare($sql);
          $stm->execute(array($number));
          $row = $stm->fetchAll(PDO::FETCH_ASSOC);

          $videos = array();
          foreach ($row as $key => $val) {
              array_push($videos, $this->getTrack($val['id']));
          }
          return $videos;
      } catch (Exception $e) {}
  }


/**
* @param integer $tracklistId list id
*
* @return array with Video objects id that are connected with given Tracklist in db
*/
	public function getTracksByTracklistId($tracklistId) {
		$tracks = array();
		$sql = 'SELECT position, tracklistid, trackid FROM trackposition WHERE tracklistid=? ORDER BY position';
        try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array($tracklistId));
            if ($row = $stm->fetchAll(PDO::FETCH_ASSOC)) {
                foreach ($row as $trackPosition) {
                    array_push($tracks, $this->getTrack($trackPosition['trackid']));
                }
            }
            return $tracks;
        } catch (Exception $e) {}
	}

/** 
* @param integer $tlid tracklist Id 
*
* @return row as associative array from db where tracklist id matches
*/
	public function getTracklistById ($tlid) {
		$sql = 'SELECT tlid, name, description, owner, private, addTime FROM tracklist WHERE tlid=?';
		$stm = $this->dbPDO->prepare($sql);
		try {$stm->execute(array ($tlid));
            $row = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $row;
        } catch (Exception $e) {}
	}

/** Get all tracklists from db own by user with id = $userId
* during process gets list of all videos in each Tracklist
*
* @param string $userId unique id of user
*
* @return an array of Tracklist objects
*/
	public function getTracklists($userId) {
		$userTracklists = array();
		$sql = 'SELECT tlid, name, description, owner, private FROM tracklist WHERE owner=?';
        try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array ($userId));
            $row = $stm->fetchAll(PDO::FETCH_ASSOC);
            foreach ($row as $tracklistData) {
                $videosInTracklist = $this->getTracksByTracklistId($tracklistData['tlid']);
                $nextTracklist = new Tracklist($tracklistData, $videosInTracklist);
                array_push($userTracklists, $nextTracklist);
            }
            return $userTracklists;
        } catch (Exception $e) {}
	}

/** Adds new tracklist to db
*
* @param string $name new tracklist name
* @param string $owner id to new tracklist owner
* @param string $description new tracklist description
*/
	public function addTracklist($name, $owner, $description=NULL) {
		$sql = 'INSERT INTO tracklist (name, description, owner, private) VALUES (?, ?, ?, "0")';
		try {
            $sth = $this->dbPDO->prepare ($sql);
            $sth->execute (array ($name, $description, $owner));
            return $this->dbPDO->lastInsertId();
        } catch (Exception $e) {}
	}

/** Update tracklist info in db
*
* @param integer $id tracklist id
* @param string $name new name
* @param string $desc new description
*/
	public function updateTracklistInfo($id,$name,$desc) {
		$sql = 'UPDATE tracklist SET addTime=UTC_TIMESTAMP(), name=?, description=? WHERE tlid=?';
		try {
            $sth = $this->dbPDO->prepare ($sql);
            $sth->execute (array ($name, $desc, $id));
        } catch (Exception $e) {}
	}

/** Removes tracklist from trackposition and tracklist tables
*
* @param integer $tracklist tracklist id
*/
	public function removeTracklist($tracklist) {
		$this->removeTracksFromList($tracklist);		// clear trackposition table
		$sql = 'DELETE FROM tracklist WHERE tlid=?';	// remove empty list
		try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array ($tracklist));
        } catch (Exception $e) {}
	}

/** Gets all videos, that belong to $owner and not on the list with given id 
* Used in edit tracklist mode to add video to playlist thats why don't want
* to have those that are allready on the tracklist under edition
*
* @param string $owner unique user id
* @param integer $excludeList to exclude from results those allready in
*
* @return array with id and name of the tracks
*/
	public function getAvailableTracksIdAndName ($owner, $excludeList=0) {
		$sql = 'SELECT track.id, track.name FROM track WHERE owner=?
			AND track.id NOT IN (SELECT trackid FROM trackposition WHERE tracklistid=?) ORDER BY track.addTime DESC';
        try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array ($owner, $excludeList));
            $userTracks = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $userTracks;
        } catch (Exception $e) {}
	}

/** Gets all videos, that belong to $owner //place for refactoring use  getAvailableTracksIdAndName($owner)
*
* @param string $owner unique user id
*
* @return array with id and name of the tracks
*/
	public function getTracksIdAndName ($owner) {
		$sql = 'SELECT name, id FROM track WHERE owner=?';
        try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array ($owner));
            $userTracks = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $userTracks;
        } catch (Exception $e) {}
	}

/** Removes from tracklistposition table that store info about tracks connected to the list
*
* @param integer $tracklist tracklist id
*
*/
	public function removeTracksFromList ($tracklist) {
		$sql = 'DELETE FROM trackposition WHERE tracklistid=?';
        try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array ($tracklist));
        } catch (Exception $e) {}
	}

/** Inserts into tracklistposition table info about tracks connected to the list
*
* @param integer $tracklist tracklist id
* @param array: keys are integers [0],... values are strings ["unique_track_id"]
*
*/
	public function addTracksToList ($tracks, $tracklist) {
        try {
            foreach ($tracks as $index=>$trackId) {
                $sql = 'INSERT INTO trackposition (position, tracklistid, trackid) VALUES (?, ?, ?)';
                $stm = $this->dbPDO->prepare($sql);
                $stm->execute(array ($index+1, $tracklist, $trackId));
            }
        } catch (Exception $e) {}
	}

/** Search through tracklist table to find matching $searchWord in tracklist.name or tracklist.description
*
* @param string $searchWord
*
* @return array, querry results as associative array
*/
	public function searchTracklist ($searchWord) {
		$sql = 'SELECT tlid, name, description, addTime  FROM tracklist
			WHERE name LIKE "%'.$searchWord.'%" OR description LIKE "%'.$searchWord.'%"
			ORDER BY addTime DESC';
		try {$stm = $this->dbPDO->prepare($sql);
            $stm->execute(array ());
            $res = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $res;
        } catch (Exception $e) {}
	}

/** Search through track table to find matching $searchWord in track.name or track.description
*
* @param string $searchWord
*
* @return array, querry results as associative array
*/
	public function searchTracks ($searchWord) {
		$sql = 'SELECT id, video, description, name, addTime FROM track
			WHERE name LIKE "%'.$searchWord.'%" OR description LIKE "%'.$searchWord.'%"
			ORDER BY addTime DESC';
		try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array ());
            $res = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $res;
        } catch (Exception $e) {}
	}

    /**
       Deletes the track, associated subtext from disk and databasen
       @param string $trackid - the track supposed to be deleted
     */
  public function removeTrack ($trackid) {
      // Track vil inneholde subtrackinfo
      $videoInf = $this->getTrack($trackid);
      $sql = 'DELETE FROM track WHERE track.id = ?';
      try {
          $stm = $this->dbPDO->prepare($sql);
          $stm->execute(array($trackid));
          if($stm->rowCount() == 0) {
              echo "kunne ikke fjerne raden fra databasen";
          }
          $stm->debugDumpParams();
          $path = explode('/', $videoInf->retPath());
          $directory = "/".$path[0]."/".$path[1]; // Mappen filene ligger i

          unlink($videoInf->retPath());
          foreach($videoInf->retSubs() as $sub => $key)
              unlink($key);
      }catch (Exception $e) {}
  }

    /**
       Updates information about a track
       @param string $trackid - id to a row getting updated
       @param string $name - name of track
       @param string $description - track description
     */
  public function updateTrack ($trackid, $name, $description) {
    $sql = 'UPDATE track SET name=?, description=? WHERE id=? LIMIT 1';
    try {
        $stm = $this->dbPDO->prepare($sql);
    $stm->execute(array($name, $description, $trackid)); 
    if($stm->rowCount() == 0) {
      echo "Kunne ikke oppdatere siden";
    }
    } catch (Exception $e) {}
  }

    /**
       Gets subtexts from the database assosiates with a certain track
       @param string $videoId - the video that needs subtexts
     */
    public function getSubtracks($videoId){
        $sql = 'SELECT textfile, subtext.name FROM subtext INNER JOIN texttotrack ON textid = subtext.id INNER JOIN track ON trackid = track.id WHERE track.id = ?';
        try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array($videoId));
            $row = $stm->fetchAll(PDO::FETCH_ASSOC);
            $subtexts = array();
            foreach($row as $r => $key) {
                $subtexts[$key['name']] = $key['textfile'];
            }

            return $subtexts;
        } catch (Exception $e) {}
    }

    /**
       Adds subtext to a track
       @param string $videoId - the video that gets new subtext
       @param string $name - name of the subtext
       @param string $path - path to the subtext-file 
     */
    public function addSubtext($videoId, $name, $path) {
        $sql = 'INSERT INTO subtext (name, textfile) VALUES (?, ?);';
        try {
            $smt = $this->dbPDO->prepare($sql);
            $smt->execute(array($name, $path));

            if($smt->rowCount() == 0) {
                die("kunne ikke legge inn tekst");
                return 0;
            }
            $textId = $this->dbPDO->lastInsertId();

            $sql = "INSERT INTO texttotrack (trackid, textid) VALUES (?, ?)";
            $smt = $this->dbPDO->prepare($sql);
            $smt->execute(array($videoId, $textId));
        } catch (Exception $e) {}
    }

    /**
       Gets one track from database. Used by videoView()
       @param string $videoid - id from database 
     */

    public function getTrack($videoId) {
      $sql = 'SELECT video, description, name, id, owner, addTime FROM track WHERE id=? LIMIT 1';
      try {
          $stm = $this->dbPDO->prepare($sql);
          $stm->execute(array($videoId));
          $row = $stm->fetch(PDO::FETCH_ASSOC);
          // Trenger en IFlokke for sjekk om man far tilbake noe
          return (new Video($row['id'], $row['video'], $row['name'], $row['description'], $row['addTime'], $this->getSubtracks($videoId)));
      } catch (Exception $e) {}
  }

    /**
       Gets tracks made by a certain user returns them as video objects
       @param string $userID - a user identification string
     */
  public function getTracksByUser($userID) {
    $sql = 'SELECT id FROM track WHERE owner=?';
    try {
        $stm = $this->dbPDO->prepare($sql);
        $stm->execute(array($userID));
        $row = $stm->fetchAll(PDO::FETCH_ASSOC);
        $videos = array();
        if($stm->rowCount() == 0) {
            echo "Fant ingen rader i databasen";
            return null;
        }

        foreach($row as $val => $key) {
            array_push($videos, $this->getTrack($key['id']));
        }
        return $videos;
    } catch (Exception $e) {}
  }

/** Finds how many tracks are in the tracklist
*
* @param integer $tracklist tracklist id
*
* @return array ([0]=>[$count]), $count is an integer value returned by querry
*/
  public function getNoOfTracks($tracklistId) {
	$sql = 'SELECT COUNT(*) FROM trackposition WHERE tracklistid=?';
    try {
    $stm = $this->dbPDO->prepare($sql);
    $stm->execute(array($tracklistId));
	$row = $stm->fetch(PDO::FETCH_NUM);
	return $row;
    } catch (Exception $e) {}
  }

    /** deletes subtext from database and server
     @param string $textPath - path to location of textfile
    */
    public function deleteSubtext($textPath) {
        $sql = 'DELETE FROM subtext WHERE textfile = ?';
        try {
            $stm = $this->dbPDO->prepare($sql);
            $stm->execute(array($textPath));
        } catch (Exception $e) {}

    }

 /** Gets $number newest videos. Used in guest homepage
*/
  public function findNewestTracks () {
    $sql = 'SELECT video, description, name, id, owner, addTime FROM track ORDER BY addTime DESC LIMIT 5';
    try {
        $stm = $this->dbPDO->prepare($sql);
        $stm->execute(array());
        $row = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    } catch (Exception $e) {}
  }


}
?>
