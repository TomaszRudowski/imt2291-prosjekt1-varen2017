<?php
/** User class contains local information about current user logged in or Guest

* based on Login class developed during WWW-Teknologi laboratories
* partly used some of solution mechanism
*/

require_once 'classes/model.php';

class User {
  protected $uid;
  protected $email;
  protected $givenname;
  protected $surename;
  protected $loggedIn;
  protected $db;

  public function __construct($DBen)
    {
      $this->db = $DBen;                      // Tar vare p� objekt fra controller
      if (isset($_GET['logout'])) {           // ALT.#1 Here after 'logout'-click
        $this->db->removePersistedLogin($_SESSION['uid']);  // Remove cookie info from DB persistant_login
        unset ($_SESSION['uid']);           // Setup as guest user
        setcookie("persistantLogin", "", time());     // Clear cookie
        header("Location: index.php");          // Remove GET['logout']

      } else if (isset($_POST['lbtn'])) {       // ALT.#2 Here if loginform was used
        //ALT #2.1 here we could divide to prcess 'login' form only
        $row = $this->db->getUserDataByEmail($_POST['email']);  // receive array FETCHed from db
        if ($row) {                   // #2.1.1 User found in db
          if (password_verify ($_POST['password'], $row['password'])) {
            $this->updateUserStatus($row['id'], $row['email'], $row['givenname'], $row['surename']);
            $this->loggedIn = true;
            if (isset($_POST['remember'])) {    // Checkbox 'remember me' was used
              $identifier =  md5($this->givenname.(time ()));
              $token = md5($this->uid.(time()));
              setcookie("persistantLogin", $this->uid.";".$identifier.";".$token, time()+60*60*24*14);
              $this->db->setPersistedLogin($this->uid,$identifier, $token); // Insert in db
            } else {
              setcookie("persistantLogin", "teskst".";"."tekst".";"."tekst", time()-5);
            }
          } else {                  // Failed password verification
            echo "<p>passord ikke verifisert</p>";
          }
        } else {                    // #2.1.2 User not found in db
          // Something
        }
        // end login form processing
        //ALT. #2.2 second part that is to be written(!) to process data from 'signup' form
      } else if (isset($_COOKIE['persistantLogin'])) {  // ALT.#3 Here if cookie was use
        $persistantData = explode(";", $_COOKIE['persistantLogin']);
        $row = $this->db->findPersistedLogin($persistantData[0], $persistantData[1]);
                                // receive array FETCHed from db
        if ($row) {                   // PersistantLogin found in db
          if ($row['token']==$persistantData[2]) {  // #3.1 Here token verifies that nobody reused cookie
            $uid = $persistantData[0];        // Prepare to update cookie
            $identifier = $persistantData[1];
            $token = md5($uid.(time()));      // Need new $token for security reasons
            setcookie("persistantLogin", $uid.";".$identifier.";".$token, time()+60*60*24*14);

            $this->db->updatePersistedLogin($uid,$identifier, $token); // Update in db

            $userData = $this->db->getUserDataById($uid); // receive array FETCHed from db
            $this->updateUserStatus($userData['id'], $userData['email'], $userData['givenname'], $userData['surename']);

          } else {                  // #3.2 BAD!! someone reused cookie on other machine?
            $this->loggedIn = false;
            $this->db->removePersistedLogin($uid);  // Remove stolen cookie from in db
          }
        }
      } else if (isset($_POST['rbtn'])) {
        $userdata = array("email" => $_POST['email'], "password" => $_POST['password'], "surname" => $_POST['surname'], "givenname" => $_POST['givenname']);
        $this->addUser($userdata);      // Update in db and updateUserStatus
      } else if (isset($_SESSION['uid']) && $_SESSION['uid'] != 'guest') {
				$row = $this->db->getUserDataById($_SESSION['uid']);
				$this->uid = $row['id'];
				$this->givenname = $row['givenname'];
				$this->surname = $row['surename'];
				$this->loggedIn = true;
				$this->email = $row['email'];
			} else {                      // ALT.#4 First time guest user
        $this->uid = NULL;
        $this->email = NULL;
        $this->givenname = NULL;
        $this->surename = NULL;
        $this->loggedIn = false;
        $_SESSION['uid'] = 'guest';
      }
  // HERE is $_SESSION['uid'] ='guest' or equal '$user->uid'. @var $db, $view, $user ready

      //$view->getHomepage();

      }   // end of __construct()

  /**
  * @param $userData associative array, should be received from 'new user' POST form
  */

    function addUser($userData) {
      $email = $userData['email'];
      print_r($userData);
      echo $userData['password'];
      $pwdHash = password_hash($userData['password'], PASSWORD_DEFAULT);
      $givenname = $userData['givenname'];
      $surename = $userData['surname'];

      $res = $this->db->addUser($email, $pwdHash, $givenname, $surename);
      if (isset($res['error'])) {             // 1. keep user as guest
        echo "<p><b>Error: </b>{$res['error']}</p>";
      } else {                      // 2. change status to logged in
        $this->updateUserStatus($res['uid'], $email, $givenname, $surename);
      }

    }

  /**
  * Most of this should be calling View class methods to print out current user status
  */
    function getUserInfo() {
      echo '<p><b>User status:</b><br>';
      if ($this->loggedIn) {
        echo $this->givenname." ".$this->surname.'<br>';
        echo $this->email.'<br>';
      } else {
        echo "Guest user</p>";
      }
    }

/** @return bool if user logged in or not
*/
  function isLoggedIn () {
    return $this->loggedIn;
  }

/** @return string that corresponds with user.id in db
*/
  function getId () {
    return $this->uid;
  }

/** @return string that corresponds with user.email in db
*/
  function getEmail () {
    return $this->email;
  }

/** @return string that corresponds with user.givenname in db
*/
  function getGivenname () {
    return $this->givenname;
  }

/** @return string that corresponds with user.surname in db
*/
  function getSurename () {
    return $this->surename;
  }

/** @return string in 'Givenname Surename' format or NULL (empty string)
*/
  function getFullName () {
    if ($this->loggedIn) {
      return $this->givenname.' '.$this->surename;
    } else {
      return NULL;
    }
  }

/** Function update information about current user, and changing status to 'Logged in'
*
* @param string $uid
* @param string $email
* @param string $givenname
* @param string $surename
*/
  function updateUserStatus($uid, $email, $givenname, $surename) {
    $this->uid = $uid;
    $this->email = $email;
    $this->givenname = $givenname;
    $this->surename = $surename;
    $this->loggedIn = true;
    $_SESSION['uid'] = $uid;
  }
}
?>
