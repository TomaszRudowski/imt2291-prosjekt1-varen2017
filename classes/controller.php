<?php
/** Controller class.
* @param $dbName to find in 'admin/settingsDbName.php' which is used by index.php
*   to create new Controller object
* @var $user User object that keep and handles local user data
* @var $db Model object that is responsible for all transaction with database
* @var $view View object that is responsible for HTML-output
* @var $tracklists array of Tracklist objects that belong to $user
*
* Controller reacts on index.php reloading with different $_POST, $_GET values
* $_POST['changeTL'] submitted change Tracklist form
* $_POST['removeTL'] clicked on Remove under tracklist info
* $_POST['addTL'] submitted Create Tracklist form
* $_GET['searchWord'] search placed in navbar submitted
* $_GET['video'] video name clicked, go to video playing
* $_GET['page'] == "addtrack" file upload page for registered users
* $_POST['editTL'] clicked on Edit under tracklist info, shows in third column edit Tracklist form
* 
*/

error_reporting(-1);
ini_set('display_errors', 'On');
require_once 'classes/user.php';
require_once 'classes/model.php';
require_once 'classes/view.php';


class Controller {
	protected $user;
	protected $db;
	protected $view;
	protected $tracklists;

	public function __construct($dbName)
    {
		$this->db = new Model($dbName);
		$this->user = new User($this->db);
		$this->view = new View();
		$this->tracklists = $this->db->getTracklists($this->user->getId());
		if (isset($_POST['changeTL'])) {
			$name = filter_var($_POST['name'],FILTER_SANITIZE_STRING);
			$desc = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
			$this->db->updateTracklistInfo($_POST['tlid'],$name,$desc);
			$this->db->removeTracksFromList($_POST['tlid']);					// clean start
			if (isset($_POST['tracksToAdd'])) {
				$this->db->addTracksToList($_POST['tracksToAdd'],$_POST['tlid']);	// add checked
			}
			$this->tracklists = $this->db->getTracklists($this->user->getId()); //update list to print on the right side

		}

		if (isset($_POST['removeTL'])) {
			$this->db->removeTracklist($_POST['tlid']);
			$this->tracklists = $this->db->getTracklists($this->user->getId()); //update list to print on the right side
		}

		if (isset($_POST['addTL'])) {
			$newListId = $this->db->addTracklist($_POST['name'], $this->user->getId(), $_POST['description']);
			if (isset($_POST['tracksToAdd'])) {
				$this->db->addTracksToList($_POST['tracksToAdd'],$newListId);	// add checked
			}
			$this->tracklists = $this->db->getTracklists($this->user->getId()); //update list
		}

		if (isset($_GET['searchWord'])) {			// show search results page
			if (isset($_GET['searchWord'])) {
				$this->prepareSearchPage(filter_var($_GET['searchWord'],FILTER_SANITIZE_STRING));
			}
		} else if (isset($_GET['video'])) {
			$video = $this->db->getTrack($_GET['video']);
			$this->view->trackView($video);
		} else if (isset($_GET['page'])) {
			if($_GET['page'] == "addtrack" && $this->user->isLoggedIn()){
			echo $this->view->header();
			echo $this->view->generateNav();
			$videos = $this->db->getTracksByUser($this->user->getId());
			if(isset($_POST['editTrack'])){
				$this->view->addTrack($videos, $_POST['editTrack']);
            } else if (isset($_POST['deleteTrack'])) {
                $this->db->removeTrack($_POST['deleteTrack']);
                $videos = $this->db->getTracksByUser($this->user->getId());
                $this->view->addTrack($videos);
            } else if (isset($_POST['updateTrack'])) {
                $this->db->updateTrack($_POST['updateTrack'], $_POST['vidname'], $_POST['viddesc']);
                $videos = $this->db->getTracksByUser($this->user->getId());
                $this->view->addTrack($videos, $_POST['updateTrack']);
            } else if (isset($_FILES["uploaded"]) || isset($_FILES["vttfil"])) {
                $this->uploadForm();
                $videos = $this->db->getTracksByUser($this->user->getId());
                $this->view->addTrack($videos);
            } else if (isset($_POST['deleteSub'])) {
                $this->db->deleteSubtext($_POST['deleteSub']);
                $videos = $this->db->getTracksByUser($this->user->getId());
                $this->view->addTrack($videos, $_POST['vidid']);
            } else {
					$this->view->addTrack($videos);
			    }
            }
		} else if($this->user->isLoggedIn()) {			// homepage - list with owned tracklists			
			echo $this->view->header();
			echo $this->view->generateNav();
			echo '<div class="row">
					<div class="col-sm-8 col-md-8">'; // col(1+2)

			if (isset( $this->tracklists)) {
				if (count($this->tracklists) > 0) {
					foreach ($this->tracklists as $index=>$tracklistObject) {
					$tracklistObject->displayTracklist();
					}
				} else {
					echo "You don't have any tracklist. Try to create one.";
				}
			}
			echo '</div>
				<div class="col-sm-4 col-md-4">';		// col(3)

			if (isset($_POST['editTL'])) {
				foreach ($this->tracklists as $index=>$tracklistObject) {

					$id = $tracklistObject->getTlid();
					if ($id == $_POST['tlid']) {
						$editData = $tracklistObject->prepareTracklistInfo();
						$ownedTracks = $this->db->getAvailableTracksIdAndName($this->user->getId(),$id);
						echo $this->view->generateEditTracklistForm($editData, $ownedTracks);
					}
				}
			} else {
				/* This part to be used later to report, in third column, submitted changes 
				if (isset($_POST['addTL'])) {		// print add raport on the left side
					echo '<p> here comes raport from adding tracklist <b>'.$_POST['name'].'</b>...</p>';
				}
				if (isset($_POST['changeTL'])) {	// print add raport on the left side
					var_dump($_POST);
					echo '<p> here comes raport from changing a tracklist <b>'.$_POST['name'].'</b>...</p>';
				} */
				$ownedTracks = $this->db->getAvailableTracksIdAndName($this->user->getId());
				echo $this->view->generateNewTracklistForm($ownedTracks);
			}
			echo '</div>
				</div>';
		} else {						// guest view
			$newestTracks = $this->db->findNewestTracks();
			echo $this->view->generateGuestHomePage($newestTracks);
			
		}
		echo $this->view->bottomLine();
	}

/** Function prepares search results to be placed in three columns.
 * All results sorted by add/update time, newest first, Column 3 sorted by position i playlist
 * Column 1 shows all single videos which name or description fits $searchWord.
 * Column 2 shows simmilar results for playlists, select playlist to see inhold
 * Column 3 shows all tracks that are in selected playlist
 * $_GET['watchList'] clicked list Name, allows to see tracks in third column
 */
	public function prepareSearchPage($searchWord) {
		echo $this->view->header();
		echo $this->view->generateNav();

		echo '<div class="row">
				<div class="col-sm-4 col-md-4">'; // col(1) for single videos
		echo '<h4>Single tracks with searchword: "'.$searchWord.'"</h4><br>';
			$tracks = $this->db->searchTracks($searchWord);
				
		foreach ($tracks as $index=>$track) {
			echo $this->view->generateTrackInfoView($track);
		}
		echo '</div>

				<div class="col-sm-4 col-md-4">'; // col(2) for list
		echo '<h4>Tracklists with searchword: "'.$searchWord.'"</h4><br>';
		$lists = $this->db->searchTracklist($searchWord);


		foreach ($lists as $index=>$tracklist) {
			$noOfVideos = $this->db->getNoOfTracks($tracklist['tlid']);
			echo $this->view->generateTracklistInfoView($tracklist,$searchWord,$noOfVideos[0]);
		}
		echo '</div>
			  <div class="col-sm-4 col-md-4" >'; // col(2) for videos in the list
			if (isset($_GET['watchList'])) {		// User has chose watch whole list from search view
				$tracklistV = $this->db->getTracklistById ($_GET['watchList']);
				$tracksV = $this->db->getTracksByTracklistId($_GET['watchList']);
				echo $this->view->generateTracklistInfoView($tracklistV['0'],$searchWord,count($tracksV));
				foreach ($tracksV as $videoModel) {
					$track = $videoModel->retAsRow();
					echo $this->view->generateTrackInfoView($track);
				}
			} else {
				echo '<p>Click on Playlist name to see here what it inholds</p>';
			}
		echo '</div>
				</div>';

	}


    public function uploadForm() {
        // Funksjonen er inspirert av w3schools eksempel på filopplastning
        // https://www.w3schools.com/php/php_file_upload.asp
        $ok=1;
        $vidOrtext = "uploaded";
        $target = "videos/";
        if (isset($_FILES['vttfil'])) {
            $vidOrtext = "vttfil";
            $target = $target . $_POST['vidid'] . "_";
        }

        $target = $target . basename($_FILES[$vidOrtext]["name"]);

        $targetSize = $_FILES[$vidOrtext]['size'];
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $uploaded_type = pathinfo($target, PATHINFO_EXTENSION);
        finfo_close($finfo);

        //This is our size condition
        if ($targetSize > 15000000) { // Over størrelse
            $ok=0;
        }

        // Kunn mp4 og vtt filer kan lastes opp. Alle andre kan ikke lastes opp
        if ($uploaded_type != 'vtt' && $uploaded_type != 'mp4') {
            $ok = 0;
        }

        if ($ok==0)  // Opplastning ikke mulig på grunn av størrelse eller type
        {
        } else {  // Filen kan lastes opp
            try {
            if(move_uploaded_file($_FILES[$vidOrtext]['tmp_name'], $target)) { // Om filen kan flyttes til en mappe
                if($vidOrtext == "uploaded")
                    $this->db->addTrack($target, $_POST['viddesc'], $_POST['vidname']);               // Legg til filen i databasen
                else
                    $this->db->addSubtext($_POST['vidid'], $_POST['vttname'], $target);
            } else {
                unset($_FILES);
            }
            } catch (Excetion $e) {
                die("Kunne ikke laste opp fil");
            }
        }
    }



}
?>