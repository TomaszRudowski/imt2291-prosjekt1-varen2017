// progress-bar scrubbing: http://blog.steveheffernan.com/2010/04/how-to-build-an-html5-video-player/
// This script formats the video and videocontrollers

var video = document.getElementById("vidspiller");

document.addEventListener("DOMContentLoaded", function() {
  // Turn off the default controls
  video.controls = false;
}, false);

function togglePlayPause() { //fra nettside
 var playpause = document.getElementById("playpause");
 if (video.paused || video.ended) {
    playpause.title = "pause";
    playpause.innerHTML = "&#10073;&#10073;";
    video.play();
 }
 else {
    playpause.title = "play";
    playpause.innerHTML = "&#9658;";
    video.pause();
 }
}
// Volum-slider
function setVolume() { //fra nettside
   var volume = document.getElementById("volume");
   video.volume = volume.value;
}
// mute-knapp
function toggleMute() {
  var volume = document.getElementById("mute");
  video.muted = !video.muted;
  if (video.muted == true) {mute.innerHTML = "&#128266;";}
  else {mute.innerHTML = "&#128263;";}
}

// Hastighets-controller
document.getElementById('hastighet').addEventListener("change", function() {
  if(this.value == "slow") {
    video.playbackRate = 0.5;
  }
  else if(this.value == "normal") {
    video.playbackRate = 1.0;
  }
  else if(this.value == "fast") {
    video.playbackRate = 1.5;
  }
});
// Add listener for updating progressbar
video.addEventListener("timeupdate", function() {
    var currentTime = video.currentTime;
    var duration = video.duration;
    var percent = Math.floor((100 / duration) * currentTime);
    document.getElementById("hp_range").style.width = percent + "%";
}, false);

// Følger med på play/pause
video.addEventListener('play', function() { //fra nettside
   var playpause = document.getElementById("playpause");
   playpause.title = "pause";
   playpause.innerHTML = "&#10073;&#10073;";
}, false);
// Følger med på play/pause
video.addEventListener('pause', function() { //fra nettside
   var playpause = document.getElementById("playpause");
   playpause.title = "play";
   playpause.innerHTML = "&#9658;";
}, false);
// Resten av funksjonene under er direkte utklipp fra nettside linket øverst.
// Her er det kun endret på variabelnavn for at funksjonaliteten skal gjelde
// for progressbaren
function setPlayProgress(clickX) {
  var newPercent = Math.max(0, Math.min(1, (clickX - findPosX(hp_slide)) / hp_slide.offsetWidth));
  video.currentTime = newPercent * video.duration
  hp_range.style.width = newPercent * (hp_slide.offsetWidth - 2)  + "px";
}

function blockTextSelection(){
  document.body.focus();
  document.onselectstart = function () { return false; };
}

function unblockTextSelection(){
  document.onselectstart = function () { return true; };
}

function findPosX(obj) {
  var curleft = obj.offsetLeft;
  while(obj = obj.offsetParent) {
    curleft += obj.offsetLeft;
  }
  return curleft;
}

hp_slide.addEventListener("mousedown", function(){

  if (video.paused) {
    videoWasPlaying = false;
  } else {
    videoWasPlaying = true;
    video.pause();
  }

  blockTextSelection();
  document.onmousemove = function(e) {
    setPlayProgress(e.pageX);
  }

  document.onmouseup = function() {
    unblockTextSelection();
    document.onmousemove = null;
    document.onmouseup = null;
    if (videoWasPlaying) {
      video.play();
    }
  }
}, true);

hp_slide.addEventListener("mouseup", function(e){
  setPlayProgress(e.pageX);
}, true);
