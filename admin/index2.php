<?php
	require_once '../classes/controller.php';		// f
	
	if (isset($_POST['dbName'])) {			// database name for development
		$dbName = $_POST['dbName'];			// new database
	} else if (isset($_GET['dbName'])) {
		$dbName = $_GET['dbName'];			// new different from default name
	} else {
		$dbName = 'test';					// default db name
	}
	
	$controller = new Controller();
?>
<html>
 <head>
  <title>index2: test controller</title>
 </head>
 <body>
  <p><a href='index.php'>Back to index</a></p>
  <p><a href='index2.php'>Reload index2 without $GET</a></p>
  <p>Testing Controller, User, Model</p>
  <p><i>using default dbName='test'</i></P>
  <form method='post' action='index.php'>
	<label for="dbName">Change DB name</label>
	<input type="text" name="dbName"/>
	<input type="submit" name = "submit" value = "Submit">
  </form>

<?php

	echo "<p><b>Current database: </b>$dbName</p>";
	$controller->getUserInfo();
	
	if (isset($_GET['createUser'])) {
		echo '<p>creating User: (ola@nordmann.com, password, Ola, Nordmann)</p>';
		$userData = array('email'=>'ola@nordmann.com', 'password'=>'password',
			'givenname'=>'Ola', 'surename'=>'Nordmann');
		$controller->addUser($userData);
		$controller->getUserInfo();
	}

?>
	
	<p><a href='index2.php?createUser=true&dbName=<?php echo $dbName ?>'>
		Create user (ola@nordmann.com, password, Ola, Nordmann) <?php echo $dbName ?></a></p>


</html>
