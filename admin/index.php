<?php
	require_once 'createEnvironment.php';		// functions to set up db, dummy data, etc.

	if (isset($_POST['dbName'])) {			// database name for development
		$dbName = $_POST['dbName'];			// new database
	} else if (isset($_GET['dbName'])) {
		$dbName = $_GET['dbName'];			// new different from default name
	} else {
		$dbName = 'test';					// default db name
	}
?>
<html>
 <head>
  <title>Set up environment for development</title>
 </head>
 <body>
  <p><a href='index.php?dbName=<?php echo $dbName ?>'>Back to index</a></p>
  <p><a href='index2.php?dbName=<?php echo $dbName ?>'>Go to index2: Controller test</a></p>
  <p>To setup local environment run: Create DB, Add Admin, Create Tables</p>
  <p><i>but feel free to experiment :)<br>using default dbName='test'</i></P>
  <form method='post' action='index.php'>
	<label for="dbName">New name</label>
	<input type="text" name="dbName"/>
	<input type="submit" name = "submit" value = "Submit">
  </form>

<?php

	echo "<p>current database $dbName</p>";

	if (isset($_GET['createDb'])) {
		echo '<p>creating db</p>';
		createLocalDatabase($dbName);
	}
	if (isset($_GET['removeDb'])) {
		echo '<p>removing db</p>';
		removeLocalDatabase($dbName);
	}
?>
   <p>Set up environment for development</p>
   <p><a href='index.php?createDb=true&dbName=<?php echo $dbName ?>'>
		Create database: <?php echo $dbName ?></a></p>
   <p><a href='index.php?removeDb=true&dbName=<?php echo $dbName ?>'>
		Remove database: <?php echo $dbName ?></a></p>

 </body>
</html>