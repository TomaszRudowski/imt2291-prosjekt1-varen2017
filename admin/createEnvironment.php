
<?php

/** Creates local environment to work with, new db, use external .sql file to create tables
 * in case you only want to add table structure to your database
 * Replace user/password if not using root without password in phpMyadmin
 * Use utf8_danish_ci format of data
 * 
 * Doesn't replace existing DB, run removeLocalDb() first
 * @author  Tomasz Rudowski
 * @version 1.0
 */
// globals to setup db
	$user = 'adminWWWproject';					
	$password = "adminPassword"; 
	$server = "localhost";
 
/* Creates PDO object to be used in all following functions
 * Change settings only here if something is not working
 * Function is used to create, remove database and admin user
 * Returns a PDO object, not linked to any specific database!! 
 */
 
function getPDOobject () {	
	$userRoot = "root";					
	$passwordRoot = "";
	 
	$server = "localhost";
	
	$db = new PDO("mysql:host=$server;", $userRoot, $passwordRoot);	
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $db;
}

/** Creates local empty database to work with
 *
 * @param string $dbName name of database to be created
 */  
function createLocalDatabase($dbName) {	
	try {	
		$db = getPDOobject();
		$sql = "CREATE DATABASE $dbName COLLATE='utf8_danish_ci'";				
		$res = $db->exec($sql);		
		echo "$res <br> Database created successfully<br>";
		addAdminUser($dbName);
		setUpTables ($dbName);
		
	} catch(PDOException $e) {		
		die ('Unable to create to database : '.$e->getMessage());
	}	
}

/** Removes local database if needed to replace corrupted data
 *
 * @param string $dbName name of database to be deleted
 */
function removeLocalDatabase($dbName) {
	try {
		$db = getPDOobject();
		$sql = "DROP DATABASE $dbName";
		$db->exec($sql);
	} catch (PDOException $e) {	
		die ('Unable to remove database : '.$e->getMessage());
	}
	
}

/* Creates admin account at $db database
 * Grant all privileges on $db.* and allow him to give priviileges to other users
 * if you were using this file to create many databases notice that user admin exist as
 * only one user with privileges to all db you created. Removing admin will not allow you 
 * to access any of them! Must add new admin account through createEnviroment interface
 *
 * @param string $dbName name of database to be get new superuser
 */

function addAdminUser ($dbName) {	
	try {
		$db = getPDOobject();
		$user = 'adminWWWproject';					
		$password = "adminPassword"; 
		
		$sql = "CREATE USER ".$user."@localhost IDENTIFIED BY '$password';"
	. "GRANT ALL PRIVILEGES ON $dbName.* TO $user@localhost WITH GRANT OPTION;"
			. "FLUSH PRIVILEGES";
		$db->exec ($sql);
	} catch (PDOException $e) {
		echo "<p>admin user exists, gets additional privileges on $dbName</p>";
		
		try {		// possible you ended up here because 'admin is a user with privileges
			$db = getPDOobject();			//in another db, skip create user, just add privileges
			$sql = "GRANT ALL PRIVILEGES ON $dbName.* TO $user@localhost WITH GRANT OPTION;"
				. "FLUSH PRIVILEGES";
			$db->exec ($sql);
		} catch (PDOException $e) {	 		// here if sth wrong
			die ('Unable to create admin user : '.$e->getMessage());
		}
		
	}
}

/* Removes admin account from all databases !!! use this to clean up
 * To code later remove privileges to $dbName instead of removing admin user ??
 *
 * @param string $dbName name of current database, not in use yet
 */

function removeAdminUser ($dbName) {	
	try {
		$db = getPDOobject();
		$user = 'adminWWWproject';		
		$admin = $db->getAdminData();
		$sql = "DROP USER $user@localhost";
		$db->exec ($sql);
	} catch (PDOException $e) {	
			die ('Unable to remove admin user : '.$e->getMessage());
	}
}

/** Connect to $dbName using admin account
 *
 * @param string $dbName name of database to be accessed as admin
 */

function databaseConnection ($dbName) {
	$user = 'adminWWWproject';					
	$password = "adminPassword"; 
	$server = "localhost";		
	$db = new PDO("mysql:host=$server;dbname=$dbName", $user, $password);	
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $db;
}

/** Create tables according last version of design, if need to change table setup
 * save dbLastVersion.sql as dbLastVersion.sql.old[#] and keep the name of current file
 *
 * @version dbLastVersion.sql version 04.02.2017
 * @param string $dbName name of database as a target to add tables
 */
 
 function setUpTables ($dbName) {
	try {
		$db = databaseConnection($dbName);
		$sql = file_get_contents('dbLastVersion.sql');		
		$db->exec ($sql); 
	} catch (PDOException $e) {	
			die ('Unable to create tables : '.$e->getMessage());
	} 
 }


?>
